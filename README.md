## Potato Scripts ##
New to Skript scripting... This is my first project. Enjoy!
Feel free to report any issue if you found one... Suggestions are welcomed ;)

Author

- TomYaMee


## Current Scripts ##
- Potato Script Loader
- Potato Help
- Potato Stats
- Potato PvP
- Potato PvP Chat
- Potato Clear Inventory
- Potato Anti Abuse (New)


## Features ##
- Potato Script Lister
- Anti Abuse Logging System
- Player's Kill/Death Stats
- PvP Rating System (LoL Inspired)
- PvP Rating Chat Prefix
- Improved inventory clearing system (*Anti Troll*)
- Killstreaks addon for PvP rating system (LoL Inspired)
- End streaks penalties (Suggested by Kelvin Ren)
- End streak money reward (LoL Inspired)
- More to come!

![](http://i1284.photobucket.com/albums/a576/TomYaMee/2013-07-24_131018_zps2ea0c51e.png)

## Links ##
- [RammyCraft Official Gaming Fan Page](https://www.facebook.com/RammyCraft "RammyCraft Official Gaming Fan Page")